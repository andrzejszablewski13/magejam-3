﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class isStrawPossed : MonoBehaviour
{
    [SerializeField] private ProtoSave _fileSave;

    void Update()
    {
        this.gameObject.GetComponent<Image>().enabled = _fileSave._possedStraw;

    }
}
