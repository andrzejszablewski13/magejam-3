﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class IfAxePossed : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]private ProtoSave _fileSave;
    
    void Update()
    {
            this.gameObject.GetComponent<Image>().enabled = _fileSave._possedPixkAce;

    }
}
