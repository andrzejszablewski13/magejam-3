﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BikPickup : MonoBehaviour
{
    [SerializeField] private ProtoSave _fileSave;
    [SerializeField] private Collider _player;

    private void OnTriggerEnter(Collider other)
    {
        if (_player)
        {
            _fileSave._collectedBik = true;
            gameObject.SetActive(false);
        }
    }
}
