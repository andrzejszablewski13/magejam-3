﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeStart : MonoBehaviour
{
    [SerializeField] private ProtoSave _fileSave;
    [SerializeField] private GameObject _player, _mucha, _tekst,_tekst2;
    private void Awake()
    {
        if(_fileSave._mucholDefited)
        {
            _mucha.SetActive(false);
            _tekst.GetComponent<RectTransform>().position = new Vector3(9999, 9999, 9999);
            _player.transform.position = new Vector3(270, _player.transform.position.y, _player.transform.position.z);
        }
        else
        {
            _tekst2.SetActive(false);
        }
    }
}
