﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveAlex : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private ProtoSave _fileSave;
   private GameObject _alex,block;
        private void Awake()
    {
        _alex = this.gameObject.GetComponentInChildren<AlexChoiceManager>().gameObject;
        block = this.gameObject.transform.GetChild(0).gameObject;
        _alex.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(_fileSave._mucholDefited)
        {
            _alex.SetActive(true);
            block.SetActive(false);
        }
    }
}
