﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Buttons : MonoBehaviour
{
    [SerializeField] private Button _newGameButton;
    [SerializeField] private Button _exitButton;
    [SerializeField] private Button _optionsButton;
    [SerializeField] private ProtoSave _fileSave;

    void Start()
    {
        _newGameButton.onClick.AddListener(PlayGame);
        _optionsButton.onClick.AddListener(Options);
        _exitButton.onClick.AddListener(QuitGame);
    }
    void Options()
    {
        SoundManagerScript.PlaySound("backToMenu");
        Application.Quit();
        
    }
    void PlayGame()
    {
        if (_fileSave._preserveItemsAfterDeath == false)
        {
            _fileSave._possedPixkAce = false;
            _fileSave._possedStraw = false;
            _fileSave._collectedBik = false;
        }
        _fileSave.RedBuschEaten = false;
        _fileSave._mucholDefited = false;
        _fileSave._whayYouDie = ProtoSave.TypeOfDeath.YouNotDie;
        SoundManagerScript.PlaySound("backToMenu");
        SceneManager.LoadScene(1);
    }
    void QuitGame()
    {
        SoundManagerScript.PlaySound("backToMenu");
       // 
    }
}