﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class EatBlueBush : MonoBehaviour, IBuschAbilities
{

    private int _stack = 0, _maxStack = 1;
    public Transform _refForRB;
    private bool _ifActive;
    public ProtoSave _fileSave;
    
    public void Eaten()
    {
        SoundManagerScript.PlaySound("crunchSound");
        if (_stack < _maxStack)
        {
            _stack++;
            _refForRB.GetComponent<Movement>()._jumpForce = _refForRB.GetComponent<Movement>()._jumpForce * 4;
            FilterControl.SetFilter1(1);
            StartCoroutine(JumpHigherTime());
        }
    }
    public void Pulled()
    {
        SoundManagerScript.PlaySound("snortSound");
        if (_ifActive == false)
        {
            _refForRB.GetComponent<Movement>()._speed = _refForRB.GetComponent<Movement>()._speed * 16;
            _ifActive = true;
            FilterControl.SetFilter1(1,0);
            StartCoroutine(SprintFasterTime());
        }
        else
        {
        }

    }
    public void Burned()
    {
        SoundManagerScript.PlaySound("burnSound");
        if (_ifActive == false)
        {
            _refForRB.GetComponent<Movement>()._speed = _refForRB.GetComponent<Movement>()._speed / 2;
            _fileSave.Achievments[0] = true;
            FilterControl.SetFilter1(0, 0);
            StartCoroutine(SprintSlowerTime());
        }
    }
    IEnumerator JumpHigherTime()
    {

        yield return new WaitForSeconds(10);
        _refForRB.GetComponent<Movement>()._jumpForce = _refForRB.GetComponent<Movement>()._jumpForce / 8;
        _stack--;
    }
    IEnumerator SprintFasterTime()
    {
        yield return new WaitForSeconds(10);
        _refForRB.GetComponent<Movement>()._speed = _refForRB.GetComponent<Movement>()._speed / 8;
        _ifActive = false;
    }
    IEnumerator SprintSlowerTime()
    {
        yield return new WaitForSeconds(10);
        _refForRB.GetComponent<Movement>()._speed = _refForRB.GetComponent<Movement>()._speed * 2;
        _ifActive = false;
    }

}
