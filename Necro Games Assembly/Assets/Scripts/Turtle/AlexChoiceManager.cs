﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AlexChoiceManager : MonoBehaviour
{
    [SerializeField] private GameObject _princessAlexGameObject;
    [SerializeField] private GameObject _regularAlexGameObject;
    [SerializeField] private GameObject _dearAlexGameObject;
    [SerializeField] private Collider _playerCollider;
    [SerializeField] private GameObject _dialogueBox,_decisionPlace;
    [SerializeField] private ProtoSave _fileSave;

    private SpriteRenderer _princessSprite;
    private SpriteRenderer _regularSprite;
    private SpriteRenderer _deadSprite;
    private bool _isPlayerInRange;
    private bool _isChoiceMade;

    private void Awake()
    {
        _princessSprite = _princessAlexGameObject.GetComponent<SpriteRenderer>();
        _regularSprite = _regularAlexGameObject.GetComponent<SpriteRenderer>();
        _deadSprite = _dearAlexGameObject.GetComponent<SpriteRenderer>();
        _isChoiceMade = false;
        _decisionPlace.SetActive(false);
    }

    private void OnTriggerEnter(Collider player)
    {
        if (_playerCollider)
        {
            _isPlayerInRange = true;
            _decisionPlace.SetActive(true);
        }
    }

    private void OnTriggerExit(Collider player)
    {
        if (_playerCollider)
        {
            _isPlayerInRange = false;
            _decisionPlace.SetActive(false);
        }
    }

    private void Update()
    {
        if (_fileSave._possedStraw)
        {
            if (Input.GetKeyDown(KeyCode.K) && _isPlayerInRange && !_isChoiceMade)
            {
                MakeAlexPrincess();
            }
            if (Input.GetKeyDown(KeyCode.O) && _isPlayerInRange && !_isChoiceMade)
            {
                KillAlex();
            }
        }
    }

    public void KillAlex()
    {
        if (_fileSave._possedStraw)
        {
            Debug.Log("You killed Alex");
            _regularSprite.enabled = false;
            _deadSprite.enabled = true;
            _dialogueBox.SetActive(false);
            _isChoiceMade = true;
            _decisionPlace.SetActive(false);
            SceneManager.LoadScene("ZZEndingTree");

        }else
        {
            MakeAlexPrincess();
        }
    }

    public void MakeAlexPrincess()
    {
        Debug.Log("You kissed Alex");
        _regularSprite.enabled = false;
        _princessSprite.enabled = true;
        _dialogueBox.SetActive(false);
        _isChoiceMade = true;
        _decisionPlace.SetActive(false);
        SceneManager.LoadScene("ZZEndingFour");
    }
}
