﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathKill : MonoBehaviour
{
    [SerializeField] private ProtoSave _saveFile;
    [SerializeField] private int _number;
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.gameObject.GetComponent<PlayerDeath>())
        {
            if(_number==0)
            {
                _saveFile._whayYouDie = ProtoSave.TypeOfDeath.DieOnBrighe;
            }
            if (_number == 1)
            {
                _saveFile._whayYouDie = ProtoSave.TypeOfDeath.DieOnWallCollide;
            }
        }
    }
}
