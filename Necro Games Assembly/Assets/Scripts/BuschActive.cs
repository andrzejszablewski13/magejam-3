﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public interface IBuschAbilities
    {
    void Eaten();
    void Pulled();
    void Burned();
    }
    
    public class BuschActive : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject _spaceForButtons;
    private GameObject[] _buttons;
    private EventTrigger[] _eventsTriggers;
    private EventTrigger.Entry[] _entriesEvents;
    private bool _inRange;
    void Awake()
    {
        _buttons = new GameObject[3];
        _eventsTriggers = new EventTrigger[3];
        _entriesEvents = new EventTrigger.Entry[3];
        for (int i = 0; i < _buttons.Length; i++)
        {
            _buttons[i]= _spaceForButtons.transform.GetChild(i).gameObject;
            _eventsTriggers[i]= _buttons[i].GetComponent<EventTrigger>();
            _entriesEvents[i] = new EventTrigger.Entry
            {
                eventID = EventTriggerType.PointerDown
            };
            switch (i)
            {
                case 0:
                    _entriesEvents[i].callback.AddListener((data) => { OnPointerDownDelegate((PointerEventData)data); });
                    
                    break;
                case 1:
                    _entriesEvents[i].callback.AddListener((data) => { OnPointerDownDelegate2((PointerEventData)data); });
                    
                    break;
                case 2:
                    _entriesEvents[i].callback.AddListener((data) => { OnPointerDownDelegate3((PointerEventData)data); });
                    
                    break;
                default:
                    break;
            }
           
        }
    
        _spaceForButtons.SetActive(false);
    }
    public void OnEnterExit(bool Enter)
    {
        if(Enter)
        {
            for (int i = 0; i < _buttons.Length; i++)
            {
                _eventsTriggers[i].triggers.Add(_entriesEvents[i]);
            }
        }
        else
        {
            for (int i = 0; i < _buttons.Length; i++)
            {
                _eventsTriggers[i].triggers.Remove(_entriesEvents[i]);
            }
        }
    }
    public void OnPointerDownDelegate(PointerEventData _)
    {
        if (_inRange)
        {
            Debug.Log("OnPointerDownDelegate Eaten1.");
            Debug.Log(this.gameObject.name);
           // this.gameObject.SendMessage("Eaten");
            this.gameObject.GetComponent<IBuschAbilities>().Eaten();
        }
    }
    public void OnPointerDownDelegate2(PointerEventData _)
    {
        if (_inRange)
        {
            Debug.Log("OnPointerDownDelegate Pulled2.");
            Debug.Log(this.gameObject.name);
            //this.gameObject.SendMessage("Pulled");
            this.gameObject.GetComponent<IBuschAbilities>().Pulled();
        }
    }
    public void OnPointerDownDelegate3(PointerEventData _)
    {
        if (_inRange)
        {
            Debug.Log("OnPointerDownDelegate Burned3.");
            Debug.Log(this.gameObject.name);
            //this.gameObject.SendMessage("Burned");
            this.gameObject.GetComponent<IBuschAbilities>().Burned();
        }    
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.GetComponent<Movement>())
        {
            _spaceForButtons.SetActive(true);
            OnEnterExit(true);
            _inRange = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.GetComponent<Movement>())
        {
            OnEnterExit(false);
            _spaceForButtons.SetActive(false);
            _inRange = false;
        }
    }
}
