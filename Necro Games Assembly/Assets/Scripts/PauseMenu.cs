﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class PauseMenu : MonoBehaviour
{
    [SerializeField] private Button _resumeButton;
    [SerializeField] private Button _mainMenuButton;
    [SerializeField] private Button _optionsInMenuButton;
    [SerializeField] private Button _backButton;

    public GameObject panel;
    public GameObject options;

    public static bool gameIsPaused;
    public static bool optionsAwaked;
    private void Start()
    {
        _resumeButton.onClick.AddListener(Resume);
        _mainMenuButton.onClick.AddListener(MainMenu);
        _optionsInMenuButton.onClick.AddListener(OptionsInMenu);
        _backButton.onClick.AddListener(BackToMenu);
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && optionsAwaked==false)
        {
            SoundManagerScript.PlaySound("pauseButton");
            if (gameIsPaused)
            {                                        
                Resume();
            }
            else
            {         
                Pause();
            }         
        }
    }

    private void Resume()
    {
        SoundManagerScript.PlaySound("backToMenu");
        panel.gameObject.SetActive(false);
        options.gameObject.SetActive(false);
        Time.timeScale = 1f;
        gameIsPaused = false;
        
    }

    private void Pause()
    {
        panel.gameObject.SetActive(true);
        Time.timeScale = 0f;
        gameIsPaused = true;

    }
    private void MainMenu()
    {
        SoundManagerScript.PlaySound("backToMenu");
        SceneManager.LoadScene(0);
    }
    private void OptionsInMenu()
    {
        SoundManagerScript.PlaySound("backToMenu");
        panel.gameObject.SetActive(false);
        options.gameObject.SetActive(true);
    
    }
   
    private void BackToMenu()
    {
        SoundManagerScript.PlaySound("backToMenu");
        options.gameObject.SetActive(!options.gameObject.activeSelf);
        panel.gameObject.SetActive(!panel.gameObject.activeSelf);

    }

}
