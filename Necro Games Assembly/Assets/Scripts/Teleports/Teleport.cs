﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Teleport : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private string _placeToTeleport;
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.GetComponent<Movement>())
        {
            SceneManager.LoadScene(_placeToTeleport);
        }
    }
}
