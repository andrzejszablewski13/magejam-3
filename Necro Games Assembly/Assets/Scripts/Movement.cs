﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEditor;

public class Movement : MonoBehaviour
{
    public float _speed = 1f;
    public float _jumpForce;
    public float _maxSpeed=50;
    private bool _isGrounded;
    private Vector3 _jump;
    private Rigidbody _rb;
    private float _jumpVector = 5.0f;
   
    private Ray _ray;
    private bool _isMoving;
    
    AudioSource audioSrc;
    private void Awake()
    {
        _rb = GetComponent<Rigidbody>();
        _jump = new Vector3(0.0f, _jumpVector, 0.0f);
        audioSrc = GetComponent<AudioSource>();
    }

    private void OnCollisionStay(Collision ground)
    {
        _isGrounded = true;
        
    }

    void Update()
    {
         
        Movements();
    }

   
    private void Movements()
    {
       
        if (_rb.velocity.x >= -1 * _maxSpeed && Input.GetAxis("Horizontal") < 0)
        {
            _rb.AddForce(Vector3.right * Input.GetAxis("Horizontal") * _speed * Time.deltaTime, ForceMode.Impulse);
        }
        else if (_rb.velocity.x <= _maxSpeed && Input.GetAxis("Horizontal") >= 0)
        {
            _rb.AddForce(Vector3.right * Input.GetAxis("Horizontal") * _speed * Time.deltaTime, ForceMode.Impulse);
        }

        if (Input.GetAxis("Horizontal") == 0 && Physics.Raycast(_ray, 0.55f))
        {
            _rb.drag = 5;
        }
        else
        {
            _rb.drag = 0;
        }
        _ray.origin = this.gameObject.transform.position;
        _ray.direction = Vector3.down;
        if (Input.GetButtonDown("Jump") && Physics.Raycast(_ray, 0.55f)&& _isGrounded)
        {
            _isGrounded = false;
            _rb.AddForce(_jump * _jumpForce);
        }
        
        if (!Physics.Raycast(_ray, 0.55f))
        {
            _rb.AddForce(Vector3.down * _jumpForce/2 * Time.deltaTime);
        }
    }

}
