﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class PurpleBuschAbi : MonoBehaviour,IBuschAbilities
{
    // Start is called before the first frame update

    private bool _ifActive;
    public ProtoSave _saveFile;
    public GameObject _player;
   
    public void Eaten()
    {
        SoundManagerScript.PlaySound("crunchSound");
        _saveFile._whayYouDie = ProtoSave.TypeOfDeath.DieFromBusch;
        _player.GetComponent<PlayerDeath>().Death();
    }
    public void Pulled()
    {
        SoundManagerScript.PlaySound("snortSound");
        if (_ifActive == false)
        {
            _ifActive = true;
            Camera.main.fieldOfView += 10;
            FilterControl.SetFilter1(7, 3,true);
            StartCoroutine(FilterControl.BackToNoraml());
            StartCoroutine(DrunkedTime());
            _saveFile.Achievments[2] = true;
        }
        else
        {

        }

    }
    public void Burned()
    {
        SoundManagerScript.PlaySound("burnSound");
        Debug.Log("Jesteś dżdżownicą ale nie ma postaci więc tylko teorretycznie");
    }
    IEnumerator DrunkedTime()
    {
        yield return new WaitForSeconds(10);
        Camera.main.fieldOfView -= 10;
        _ifActive = false;
    }
    
}

