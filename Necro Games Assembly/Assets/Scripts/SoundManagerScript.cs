﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManagerScript : MonoBehaviour
{
    public static AudioClip crunchSound, damage1, damage2, damage3, deathSound, flamethrowerSound, laughSound, burnSound, snortSound, step1, step2, step3, backToMenu, menuButtons, newGame, pause, pauseButton;
    static AudioSource audioSrc;
    [SerializeField] private ProtoSave _fileSave;
    public void Start()
    {
        crunchSound = Resources.Load<AudioClip>("crunchSound");
        burnSound = Resources.Load<AudioClip>("burnSound");
        snortSound = Resources.Load<AudioClip>("snortSound");

        backToMenu = Resources.Load<AudioClip>("backToMenu");
        menuButtons = Resources.Load<AudioClip>("menuButtons");
        newGame = Resources.Load<AudioClip>("newGame");
        pause = Resources.Load<AudioClip>("pause");
        pauseButton = Resources.Load<AudioClip>("pauseButton");

        audioSrc = GetComponent<AudioSource>();
        
    }

    private void Update()
    {
        audioSrc.volume = _fileSave._volume;
    }

    public static void PlaySound (string clip)
    {
        switch (clip)
        {
            case "crunchSound":
                audioSrc.PlayOneShot(crunchSound);
                break;
            case "burnSound":
                audioSrc.PlayOneShot(burnSound);
                break;
            case "snortSound":
                audioSrc.PlayOneShot(snortSound);
                break;
            case "backToMenu":
                audioSrc.PlayOneShot(backToMenu);
                break;
            case "menuButtons":
                audioSrc.PlayOneShot(menuButtons);
                break;
            case "newGame":
                audioSrc.PlayOneShot(newGame);
                break;
            case "pause":
                audioSrc.PlayOneShot(pause);
                break;
            case "pauseButton":
                audioSrc.PlayOneShot(pauseButton);
                break;

        }
    }
}
