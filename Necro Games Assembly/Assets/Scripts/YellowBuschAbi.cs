﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YellowBuschAbi : MonoBehaviour, IBuschAbilities
{
    private int _stack=0,_maxStack=2;
    public Transform _refForRB;
    private Rigidbody _rB;
    private bool _ifActive;
    private void Awake()
    {
        _rB = _refForRB.GetComponent<Rigidbody>();
    }
    public void Eaten()
    {
        SoundManagerScript.PlaySound("crunchSound");
        Debug.Log(_stack);
        if(_stack<= _maxStack)
        {
            _stack++;
            _refForRB.GetComponent<Movement>()._jumpForce = _refForRB.GetComponent<Movement>()._jumpForce * 2;
            FilterControl.SetFilter1(15, 2);
            StartCoroutine(JumpHigherTime());
        }
    }
    public void Pulled()
    {
        SoundManagerScript.PlaySound("snortSound");
        if (_ifActive == false)
        {
            _refForRB.GetComponent<Movement>()._speed = _refForRB.GetComponent<Movement>()._speed * 2;
              _ifActive = true;
            FilterControl.SetFilter1(21, 2);
            StartCoroutine(SprintFasterTime());
        }else
        {
        
        }

    }
    public void Burned()
    {
        SoundManagerScript.PlaySound("burnSound");
        FilterControl.SetFilter1(18, 3);
        Debug.Log("Coś się stało…");
        StartCoroutine(FilterControl.BackToNoraml());
    }
    IEnumerator JumpHigherTime()
    {

        yield return new WaitForSeconds(10);
        _refForRB.GetComponent<Movement>()._jumpForce = _refForRB.GetComponent<Movement>()._jumpForce / 2;
        _stack--;
    }
    IEnumerator SprintFasterTime()
    {

        yield return new WaitForSeconds(10);
        _refForRB.GetComponent<Movement>()._speed = _refForRB.GetComponent<Movement>()._speed / 2;
        _ifActive = false;
     }
}
