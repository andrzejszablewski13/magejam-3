﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class PlayerDeath : MonoBehaviour
{
    public ProtoSave _fileSave;
    // Start is called before the first frame update
    public void Death()
    {
        _fileSave._possedPixkAce = false;
        _fileSave._possedStraw = false;
        _fileSave._collectedBik = false;
        _fileSave.RedBuschEaten = false;
        SceneManager.LoadScene(1);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.gameObject.layer == LayerMask.NameToLayer("KILL"))
        {
            Death();
        }
    }
}
