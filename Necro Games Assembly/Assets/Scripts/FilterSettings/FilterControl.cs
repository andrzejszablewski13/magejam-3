﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FilterControl : MonoBehaviour
{
    // Start is called before the first frame update
    private static ParticleSystem _parSystem;
    private static ReflectionProbe _refProbbe;
    private static MeshRenderer _meshRender;
    private static Object[] _materials;
    void Awake()
    {

        _materials = Resources.LoadAll("Materials",typeof(Material)) ;
        _refProbbe = this.gameObject.GetComponent<ReflectionProbe>();
        _meshRender = this.gameObject.GetComponent<MeshRenderer>();
        _parSystem = this.gameObject.GetComponent<ParticleSystem>();
        _parSystem.Stop();
        _refProbbe.intensity = 2;
        _meshRender.material = (Material)_materials[0];
    }

    // Update is called once per frame
    public static void SetFilter1(int _numberOfMaterial=0,int _intensity=2,bool _playParticleIfTrue=false)
    {
        if (_playParticleIfTrue)
        {
            _parSystem.Play();
        }
        else
        {
            _parSystem.Stop();
        }
        _refProbbe.intensity = _intensity;
        _meshRender.material = (Material)_materials[_numberOfMaterial];

    }
    public static IEnumerator BackToNoraml()
    {
        yield return new WaitForSeconds(3f);
        SetFilter1();
    }
}
