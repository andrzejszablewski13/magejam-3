﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class QuickTimeEvent : MonoBehaviour
{
    private Rigidbody _rB;
    private bool _enemyAttack;//if false is attack
    [SerializeField] private float _timeForAction=6f;
    [SerializeField] private GameObject _dodgeBut, _AttackButt,_fireControlPlace,_mucholAI,playerAnimator,_Muscha;
    [SerializeField] private Image _hPBar;
    [SerializeField] private MeshRenderer _filter;
    [SerializeField] private Material _SecondFase,_end;
    [SerializeField] private ProtoSave _fileSave;
    private Object[] _hPBarStage;
    private IEnumerator _corutin;
    private FireControl.Decision _decidedMove;
    private int _hPStage=0;
    private void Awake()
    {
        _rB = this.gameObject.GetComponentInParent<Rigidbody>();
        _corutin = TimeForDecide();
        //__corutin2 = TurnsSystem();
        _hPBarStage=Resources.LoadAll("Sprites/HPBar", typeof(Sprite));

        StartCoroutine(_corutin);
    }
    public void TurnsSystem()
    {
        if (_fileSave._possedPixkAce && _fileSave._collectedBik)
        { _decidedMove = _fireControlPlace.GetComponent<FireControl>().OnTurn(); }
        else
        {
            _decidedMove = FireControl.Decision.nothing;
        }
        Debug.Log(_decidedMove);
        _enemyAttack=_mucholAI.GetComponent<MuchaAI>().MuchaTurn();
        if(_enemyAttack&& _decidedMove!=FireControl.Decision.dodge)
        {
            Debug.Log("YouDie");
            SceneManager.LoadScene("ZZEndingOne");
        }else if(_enemyAttack==false && _decidedMove== FireControl.Decision.attack && _hPStage < 4)
        {
            Debug.Log("Attack");
            _hPStage++;
           if(_hPStage<5) _hPBar.sprite =(Sprite) _hPBarStage[_hPStage];
            _timeForAction--;
        }
        if(_hPStage==3)
        {
            _filter.material = _SecondFase;
        }
        if (_hPStage < 5)
        {
            StartCoroutine(_corutin);
            StartCoroutine(TimeForDecide());
        }
        else
        {
            _hPBar.enabled = false;
            _filter.material = _end;
            _Muscha.SetActive(false);
            _fileSave._mucholDefited = true;
            _fireControlPlace.GetComponentInParent<Movement>().enabled = true;
            _fireControlPlace.GetComponentInParent<Movement>()._speed = 25;
            playerAnimator.GetComponentInParent<PlayerAnimator>().enabled = true;
        }
    }
    public void Decided()
    {
        //StopCoroutine(_corutin);
        //StartCoroutine(__corutin2);
        //TurnsSystem();
        
    }
    public IEnumerator TimeForDecide()
    {

        _AttackButt.SetActive(true);
        _dodgeBut.SetActive(true);
        _AttackButt.GetComponent<RectTransform>().localScale=Vector3.one;
        _dodgeBut.GetComponent<RectTransform>().localScale = Vector3.one;
        
        _AttackButt.GetComponent<Animator>().speed = 1 / _timeForAction;
        _dodgeBut.GetComponent<Animator>().speed = 1 / _timeForAction;
        _dodgeBut.GetComponent<Animator>().SetTrigger("Activaate");
        _AttackButt.GetComponent<Animator>().SetTrigger("Activaate");
        _AttackButt.GetComponent<Animator>().Play("New State", 0);
        _dodgeBut.GetComponent<Animator>().Play("New State", 0);
        yield return new WaitForSeconds(_timeForAction);
        //StartCoroutine(__corutin2);
        TurnsSystem();
    }
    

}
