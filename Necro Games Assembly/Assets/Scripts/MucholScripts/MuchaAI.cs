﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MuchaAI : MonoBehaviour
{
    // Start is called before the first frame update
    private int _turn = -1;
    [SerializeField] private GameObject _muszka,_player;
    [SerializeField] private Object[] _muszkaFight;
    private SpriteRenderer _spiteRender;

    private void Awake()
    {
        _muszkaFight = Resources.LoadAll("Sprites/Muchol/ver3", typeof(Sprite));
        _spiteRender = _muszka.GetComponent<SpriteRenderer>();
    }

    public bool MuchaTurn()
    {
        _turn++;
        if(_turn>3)
        {
            _turn = 0;
        }
        switch (_turn)
        {
            case 0:
                //_muszka.transform.localRotation = new Quaternion(0, 0, 0, 0);
                _spiteRender.sprite =(Sprite) _muszkaFight[_muszkaFight.Length - 1];
                break;
            case 1:
                // _muszka.transform.localRotation = new Quaternion(0, 0, 0, 0);
                _spiteRender.sprite = (Sprite)_muszkaFight[0];
                _muszka.transform.localPosition=new Vector3(-20,0,0);
                _player.transform.localRotation = new Quaternion(0, 180, 0, 0);
                return true;
                break;
            case 2:
                // _muszka.transform.localRotation = new Quaternion(45, 180, 0, 0);
                _spiteRender.sprite = (Sprite)_muszkaFight[_muszkaFight.Length - 1];
                break;
            case 3:
                //_muszka.transform.localRotation = new Quaternion(0, 180, 0, 0);
                _spiteRender.sprite = (Sprite)_muszkaFight[0];
                _muszka.transform.localPosition = new Vector3(20, 0, 0);
                _player.transform.localRotation = new Quaternion(0, 0, 0, 0);
                return true;
                break;
            default:
                break;
        }
        return false;
    }
}
