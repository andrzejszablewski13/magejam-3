﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class FireControl : MonoBehaviour
{
    public enum Decision
    {
        attack,dodge,nothing
    }
    // Start is called before the first frame update
    private Rigidbody _rB;
    private SpriteRenderer _spriteRend;
    [SerializeField] private GameObject _dodgeBut, _AttackButt,_player;
    private Decision _whatDecided= Decision.nothing;

    private void Awake()
    {
        _rB = this.gameObject.GetComponentInParent<Rigidbody>();
        _spriteRend= this.gameObject.GetComponent<SpriteRenderer>();
        _spriteRend.enabled = false;
    }
    
    public void AttackDecide(bool _isTrue)
    {
        if(_isTrue)
        {
            _whatDecided = Decision.attack;
        }
        else
        {
            _whatDecided = Decision.dodge;
        }
        _dodgeBut.SetActive(false);
        _AttackButt.SetActive(false);
    }
    public Decision OnTurn()
    {
        switch (_whatDecided)
        {
            case Decision.attack:
                FireOn();
                _whatDecided = Decision.nothing;
                return Decision.attack;
                break;
            case Decision.dodge:
                Dogde();
                _whatDecided = Decision.nothing;
                return Decision.dodge;
                break;
            case Decision.nothing:
                _whatDecided = Decision.nothing;
                return Decision.nothing;
                break;
            default:
                break;
        }
        _whatDecided = Decision.nothing;
        return Decision.nothing;
    }
    private void FireOn()
        {
        _spriteRend.enabled = true;
        _rB.mass = 1000;
        StartCoroutine(FireOff());
    }
    private void Dogde()
    {
        //_rB.AddForce(Vector3.up * _jumpForce,ForceMode.Impulse);
        _player.gameObject.transform.position = new Vector3(_player.gameObject.transform.position.x,8,_player.gameObject.transform.position.z);
    }
    IEnumerator  FireOff()
    {
        yield return new WaitForSeconds(1);
        _rB.mass = 1;
        _spriteRend.enabled = false;
    }

}
