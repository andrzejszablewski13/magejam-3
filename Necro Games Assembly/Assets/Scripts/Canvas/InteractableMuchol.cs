﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableMuchol : MonoBehaviour
{
    public delegate void DialogueTrigger(Dialogue dialogue);
    public static event DialogueTrigger dialogueMucholTriggered;
    public static event DialogueTrigger dialogueMucholLeft;

    public Dialogue dialogue;

    private void OnTriggerEnter(Collider player)
    {
        dialogueMucholTriggered(dialogue);
    }

    private void OnTriggerExit(Collider player)
    {
        dialogueMucholLeft(dialogue);
    }
}
