﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DialogueManager: MonoBehaviour
{
    [SerializeField] private GameObject _textBoxMuchol;
    [SerializeField] private GameObject _textBoxAlex;
    public TextMeshProUGUI _textAlex;
    public TextMeshProUGUI _textMuszor;
    private Queue<string> _sentences;

    private bool _isMuszorTalking;
    private bool _isAlexTalking;

    private void OnEnable()
    {
        InteractableMuchol.dialogueMucholTriggered += StartMucholLine;
        InteractableMuchol.dialogueMucholLeft += ExitDialogue;

        InteractableAlex.dialogueAlexTriggered += StartAlexLine;
        InteractableAlex.dialogueAlexLeft += ExitDialogue;
    }

    private void OnDisable()
    {
        InteractableMuchol.dialogueMucholTriggered -= StartMucholLine;
        InteractableMuchol.dialogueMucholLeft -= ExitDialogue;

        InteractableAlex.dialogueAlexTriggered -= StartAlexLine;
        InteractableAlex.dialogueAlexLeft -= ExitDialogue;
    }

    private void Awake()
    {
        _sentences = new Queue<string>();
    }

    private void StartAlexLine(Dialogue dialogue)
    {
        _isAlexTalking = true;
        _textBoxAlex.SetActive(true);

        _sentences.Clear();

        foreach (string sentence in dialogue.sentences)
        {
            _sentences.Enqueue(sentence);
        }

        DisplayNextSentence();
    }

    private void StartMucholLine(Dialogue dialogue)
    {
        _isMuszorTalking = true;
        _textBoxMuchol.SetActive(true);

        _sentences.Clear();

        foreach  (string sentence in dialogue.sentences)
        {
            _sentences.Enqueue(sentence);
        }

        DisplayNextSentence();
    }

    public void DisplayNextSentence()
    {
        if (_sentences.Count == 0)
        {
            EndDialogue();
            return;
        }

        string sentence =_sentences.Dequeue();

        if (_isMuszorTalking == true)
        {
            _textMuszor.text = sentence;
        }

        if (_isAlexTalking == true)
        {
            _textAlex.text = sentence;
        }
    }

    private void ExitDialogue(Dialogue dialogue)
    {
        _isAlexTalking = false;
        _isMuszorTalking = false;
        _textBoxMuchol.SetActive(false);
        _textBoxAlex.SetActive(false);
    }

    private void EndDialogue()
    {
        _isAlexTalking = false;
        _isMuszorTalking = false;
        _textBoxMuchol.SetActive(false);
        _textBoxAlex.SetActive(false);
    }
}
