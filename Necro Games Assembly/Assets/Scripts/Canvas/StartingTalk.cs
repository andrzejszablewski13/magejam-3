﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartingTalk : MonoBehaviour
{
    [SerializeField] private ProtoSave _filesave;
    private Dialogue _talk;
    void Awake()
    {
        _talk = new Dialogue();
        _talk.sentences = new string[1];
        switch (_filesave._whayYouDie)
        {
            case ProtoSave.TypeOfDeath.YouNotDie:
                _talk.sentences[0] = "Hi I am Muchol. Welcome in magical place.";
                break;
            case ProtoSave.TypeOfDeath.DieOnBrighe:
                _talk.sentences[0] = "Not all is Solid as it seems to be";
                break;
            case ProtoSave.TypeOfDeath.DieOnWallCollide:
                _talk.sentences[0] = "This is dangerous place";
                break;
            case ProtoSave.TypeOfDeath.DieFromBusch:
                _talk.sentences[0] = "Some sweets can be poisonus";
                break;
            default:
                break;
        }
        _filesave._whayYouDie = ProtoSave.TypeOfDeath.YouNotDie;
        this.gameObject.GetComponent<InteractableMuchol>().dialogue = _talk;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
