﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderControl : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private ProtoSave _fileSave;
    private Slider _slider;
    void Start()
    {
        _slider = this.gameObject.GetComponent<Slider>();
    }

    public void SetValueOfVolume()
    {
        _fileSave._volume = _slider.value;
    }
}
