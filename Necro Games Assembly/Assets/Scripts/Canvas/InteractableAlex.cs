﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableAlex : MonoBehaviour
{
    public delegate void DialogueTrigger(Dialogue dialogue);
    public static event DialogueTrigger dialogueAlexTriggered;
    public static event DialogueTrigger dialogueAlexLeft;

    public Dialogue dialogue;

    private void OnTriggerEnter(Collider player)
    {
        dialogueAlexTriggered(dialogue);
    }

    private void OnTriggerExit(Collider player)
    {
        dialogueAlexLeft(dialogue);
    }
}
