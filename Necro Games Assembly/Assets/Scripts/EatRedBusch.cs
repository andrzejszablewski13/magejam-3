﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class EatRedBusch : MonoBehaviour, IBuschAbilities
{
    public ProtoSave _saveFile;
    public GameObject _player;
    public void Eaten()
    {
        SoundManagerScript.PlaySound("crunchSound");
        if (_saveFile.RedBuschEaten == false)
        {
            _saveFile.RedBuschEaten = true;
            _saveFile._whayYouDie = ProtoSave.TypeOfDeath.DieFromBusch;
            _player.GetComponent<PlayerDeath>().Death();
        }
    }
    public void Pulled()
    {
        SoundManagerScript.PlaySound("snortSound");
        if (_saveFile._possedStraw == false)
        {
            _saveFile._possedStraw = true;
        }
    }
    public void Burned()
    {
        SoundManagerScript.PlaySound("burnSound");
        FilterControl.SetFilter1(0, 5);
        Debug.Log("test");
        StartCoroutine(FilterControl.BackToNoraml());
    }
    
}
