﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimator : MonoBehaviour
{
    // Start is called before the first frame update
    public enum StateOfAnimation
    {
        Idle,MoveRight,MoveLeft, JumpStart,Jump
    }
    private Object[] _AnimationIdleStay, _AnimationIdleIn, _AnimationIdleOut,_AnimationMoveRight, _AnimationMoveLeft,_AnimationJumpLeft,_AnimationJumpRight;
    private Rigidbody _Rb;
    private SpriteRenderer _spirteRnd;
    private int _numberOfSprite=0,_numberOfCycle=0,_numberOfJumpCycle=0;
    [SerializeField] [Range (1,100)] private int _cycleTochangeIdleStay = 30, _cycleTochangeMove = 10, _cycleTochangeJump = 10;
    [SerializeField] private float _precisionOfAnimation = 0.01f;
    private StateOfAnimation stageOfAnimation;
    private Ray _ray;
    private bool _isJumpLeft=true;
    void Start()
    {
        _AnimationIdleStay= Resources.LoadAll("Sprites/Idle/Stay_Idle",typeof(Sprite));
        _AnimationIdleIn = Resources.LoadAll("Sprites/Idle/In_Idle", typeof(Sprite));
        _AnimationIdleOut = Resources.LoadAll("Sprites/Idle/Out_Idle", typeof(Sprite));
        _AnimationMoveRight = Resources.LoadAll("Sprites/Move/Move_Right", typeof(Sprite));
        _AnimationMoveLeft = Resources.LoadAll("Sprites/Move/Move_Left", typeof(Sprite));
        _AnimationJumpLeft = Resources.LoadAll("Sprites/Jump/Jump_Left", typeof(Sprite));
        _AnimationJumpRight = Resources.LoadAll("Sprites/Jump/Jump_Right", typeof(Sprite));
        _Rb = this.gameObject.GetComponentInParent<Rigidbody>();
        _spirteRnd= this.gameObject.GetComponent<SpriteRenderer>();
        stageOfAnimation = StateOfAnimation.Idle;
        _ray.origin = this.gameObject.transform.position;
        _ray.direction = Vector3.down;
        StartCoroutine(AnimationCorutine());
    }

    // Update is called once per frame
    
    IEnumerator AnimationCorutine()
    {
        while (true)
        {
            if (_Rb.velocity.y!=0)
            {
                stageOfAnimation = StateOfAnimation.Jump;
                _numberOfCycle = 99;
                
            }
            else
            {
                _numberOfJumpCycle = 0;
                if (_Rb.velocity.x > 0)
                {
                    if (stageOfAnimation != StateOfAnimation.MoveRight)
                    {
                        stageOfAnimation = StateOfAnimation.MoveRight;
                        _numberOfCycle = 99;
                    }
                }
                else if (_Rb.velocity.x < 0)
                {
                    if (stageOfAnimation != StateOfAnimation.MoveLeft)
                    {
                        stageOfAnimation = StateOfAnimation.MoveLeft;
                        _numberOfCycle = 99;
                    }
                }
                else
                {
                    if (stageOfAnimation != StateOfAnimation.Idle)
                    {
                        stageOfAnimation = StateOfAnimation.Idle;
                        _numberOfCycle = 99;
                    }
                }
            }
            _numberOfCycle++;
            switch (stageOfAnimation)
            {
                case StateOfAnimation.Idle:
                    ChangeSprite(_cycleTochangeIdleStay, _AnimationIdleStay);
                    break;
                case StateOfAnimation.MoveRight:
                    ChangeSprite(_cycleTochangeMove, _AnimationMoveRight);
                    break;
                case StateOfAnimation.MoveLeft:        
                        ChangeSprite(_cycleTochangeMove, _AnimationMoveLeft);
                    break;
                case StateOfAnimation.Jump:
                    if (_numberOfCycle > _cycleTochangeJump)
                    {
                        if (_numberOfSprite >= _AnimationJumpLeft.Length)
                        {
                            _numberOfSprite=_AnimationJumpLeft.Length-1;
                        }
                        _numberOfCycle = 0;
                        if (_Rb.velocity.x > 0)
                        {
                            _spirteRnd.sprite = (Sprite)_AnimationJumpRight[_numberOfSprite];
                        }
                        else
                        {
                            _spirteRnd.sprite = (Sprite)_AnimationJumpLeft[_numberOfSprite];
                        }
                            
                        _numberOfSprite++;
                        
                        //_numberOfSprite = _numberOfSprite < _AnimationJumpLeft.Length ? _numberOfSprite+=1 : _numberOfSprite;
                    }
                        break;
                default:
                    break;
            }
           
            yield return new WaitForSeconds(_precisionOfAnimation);
        }
    }
    private void ChangeSprite(int _cycletoChange,object[] _animationSprites)
    {
        if (_numberOfCycle > _cycleTochangeJump)
        {
            _numberOfCycle = 0;
            if (_numberOfSprite >= _animationSprites.Length)
            {
                _numberOfSprite = 0;
            }
            _spirteRnd.sprite = (Sprite)_animationSprites[_numberOfSprite];

            _numberOfSprite++;
        }
    }
}
