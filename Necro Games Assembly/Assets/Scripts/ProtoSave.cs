﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SaveDataFile", menuName = "ProtoSave")]
public class ProtoSave : ScriptableObject
{
    public enum NamesOfAcievements
    {
        High_on_Life, Get_Started, Glaucoma
    }
    public enum TypeOfDeath
    {
        YouNotDie,DieOnBrighe,DieOnWallCollide,DieFromBusch
    }
    public bool RedBuschEaten = false;
    public bool[] Achievments = new bool[3];
    public bool _preserveItemsAfterDeath=false;
    public bool _possedStraw = false;
    public bool _possedPixkAce = false;
    public bool _collectedBik = false;
    public bool _mucholDefited = false;
    public float _volume = 1;
    public TypeOfDeath _whayYouDie;
    public void SetItemsAfterDeath()
    {
        _preserveItemsAfterDeath = !_preserveItemsAfterDeath;
    }

}
