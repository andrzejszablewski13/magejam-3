﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnailTransform : MonoBehaviour
{
    public bool SnailHadTransform=true;
    private Rigidbody _rB;
    private Movement _refToMove;
    private float _jump, _speeed;
	private bool _changed=false;

    private void Awake()
    {
        _rB = this.gameObject.GetComponent<Rigidbody>();
        _refToMove = this.gameObject.GetComponent<Movement>();
    }
    private void OnCollisionEnter(Collision collision)
    {
        if(SnailHadTransform && collision.collider.gameObject.layer==LayerMask.NameToLayer("move"))
        {
            _rB.useGravity = false;
            _speeed = _refToMove._speed;
            _jump = _refToMove._jumpForce;
            _refToMove._jumpForce = 0;
            _refToMove._speed = 2;
		_changed=true;
            
        }
    }
    private void OnCollisionExit(Collision collision)
    {
        if (SnailHadTransform && _changed && collision.collider.gameObject.layer == LayerMask.NameToLayer("move"))
        {
            _rB.useGravity = true;
             _refToMove._speed= _speeed;
             _refToMove._jumpForce=_jump;
		_changed=false;
        }
    }
    
    private void OnCollisionStay(Collision collision)
    {
        if (SnailHadTransform==false && collision.collider.gameObject.layer == LayerMask.NameToLayer("move"))
        {
            _rB.useGravity = true;
            _refToMove._speed = _speeed;
            _refToMove._jumpForce = _jump;
		_changed=false;
        }
    }
}
